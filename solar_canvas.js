var canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");
//Creation of orbit
function Orbit(xpos,ypos,xrad,yrad){
	this.xpos=xpos;
	this.ypos=ypos;
	this.xrad=xrad;//orbital radius (a)
	this.yrad=yrad;//orbital radius (b)
	this.draw_orbit = function(){
		c.beginPath();
		c.ellipse(this.xpos,this.ypos,this.xrad,this.yrad,0,Math.PI*2 , false);
		c.setLineDash([5,10]);
		c.strokeStyle = 'rgba(255,255,255,0.2)';
		c.stroke();
	}
}
//Planet creation and revolution
function Planet(orbit_number,radius,path_x,path_y,speed,color)
{
	this.speed=speed;
	this.path_x=path_x;
	this.path_y=path_y;
	this.cent_x=950;
	this.cent_y=500;
	this.init_x_rad = 200;//radius of first path, a & b
	this.init_y_rad = 80;
	this.rate_of_change_x = 90;//increase in radius of subsequent paths, a & b
	this.rate_of_change_y = 50;
	this.color = color;
	this.orbit_number=orbit_number;
	this.a = (this.orbit_number-1)*this.rate_of_change_x+this.init_x_rad;
	this.b = (this.orbit_number-1)*this.rate_of_change_y+this.init_y_rad;
	
	this.radius=radius;
	this.radians = Math.random()*Math.PI*2;
	this.draw_planet = function(){
		if(orbit_number==6)//rings for saturn
		{
			c.beginPath();//outer white rings
			c.ellipse(this.path_x,this.path_y,30,20,0,Math.PI*2,false);
			c.fillStyle='white';
			c.fill();
			c.closePath();
			c.beginPath();//inner black space
			c.ellipse(this.path_x,this.path_y,24,16,0,Math.PI*2,false);
			c.fillStyle='black';
			c.fill();
			c.beginPath();//adding 2 strokes for better visual
			c.ellipse(this.path_x,this.path_y,27,18,0,Math.PI*2,false);
			c.strokeStyle='black';
			c.setLineDash([]);
			c.stroke();
			c.beginPath();

			c.ellipse(this.path_x,this.path_y,28.5,19,0,Math.PI*2,false);
			c.strokeStyle='black';
			c.setLineDash([]);
			c.stroke();
		}
			c.beginPath();
			c.arc(this.path_x,this.path_y,this.radius,0,Math.PI*2 , false);
			c.fillStyle = this.color;
			c.fill();
		
		
		
	}

	this.revolve = function(){
		this.radians+=this.speed;

		this.path_x = this.cent_x+ this.a*Math.cos(this.radians);
		this.path_y = this.cent_y+ this.b*Math.sin(this.radians);
		this.draw_planet();

	}
}
//Center of sun co-ordinates
var xpos = 950;
var ypos = 500;

//Star definition and functions
function Stars(x,y,radius)
{
	this.x=x;
	this.y=y;
	this.radius=radius;
	this.draw_star = function(){
		c.beginPath();
		c.arc(this.x,this.y,this.radius,0,Math.PI*2 , false);
		c.fillStyle = 'white';
		c.fill();
	}
	this.change_rad = this.radius*0.1;
	
	this.blink = function(){

		this.radius+=this.change_rad;
		if(this.radius>2)
		{
			this.change_rad = -this.change_rad;
			this.radius = 2;
		}
		if(this.radius<0)
		{
			this.change_rad = -this.change_rad;
			this.radius = this.change_rad;
		}
		this.draw_star();
	}
}
//First orbit parameters
var xrad = 200;
var yrad = 80;
//Increase in orbit size
var rate_change_x = 90;
var rate_change_y = 50;
//List of orbits
var orbits = [];
//Creation of list of orbits
for(var i=0;i<9;i++)
{
	orbits.push(new Orbit(xpos,ypos,xrad,yrad));
	xrad+=rate_change_x;
	yrad+=rate_change_y;
}
var planets = [];
//Reinitialising initial x-radius of first orbit
xrad = 200;
//Manually define planet properties into list
planets.push(new Planet(1,5,xpos+xrad,ypos,0.09,'#7B241C'));
planets.push(new Planet(2,10,xpos+xrad,ypos,0.014,'#873600'));
planets.push(new Planet(3,11,xpos+xrad,ypos,0.015,'#1F618D'));
planets.push(new Planet(4,4,xpos+xrad,ypos,0.013,'#D98880'));
planets.push(new Planet(5,20,xpos+xrad,ypos,0.012,'#E59866'));
planets.push(new Planet(6,15,xpos+xrad,ypos,0.015,'#F7DC6F'));
planets.push(new Planet(7,15,xpos+xrad,ypos,0.019,'#2E86C1'));
planets.push(new Planet(8,15,xpos+xrad,ypos,0.02,'#5D6D7E'));
planets.push(new Planet(9,6,xpos+xrad,ypos,0.025,'#99A3A4'));
//Creation of star list
var stars = [];
for(var i=0;i<150;i++)
{
	stars.push(new Stars(Math.random()*window.innerWidth,Math.random()*window.innerHeight,Math.random()*2));
}
//Drawing Sun
function draw_sun(){
	c.beginPath();
	c.arc(950,500,60,0,Math.PI*2 , false);
	c.fillStyle = 'yellow';
	c.fill();
	c.closePath();
}

//Function to animate revolution
function animate() {
	 requestAnimationFrame(animate);
	c.clearRect(0,0,innerWidth,innerHeight);
	for(var i=0;i< orbits.length;i++)
	{
		orbits[i].draw_orbit();
		planets[i].revolve();
	}

	draw_sun();

	for(var i=0;i<150;i++)
	{
		stars[i].blink();
	}
}
animate();

